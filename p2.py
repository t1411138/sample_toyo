import random
#取引クラス
class TradeInf:
    def __init__(self,A,B,rate,N):
        self.A = A #Aさんの資産
        self.B = B #Bさんの資産
        self.rate = rate #移動する資産の％
        self.N = N #取引回数

    def social_transaction(self,x):
        #N回取引するメソッド
        #変更点２：社会を追加する。
        #取引のX%を徴収する税制度を導入
        for i in range(self.N):
            gosa = min(self.A,self.B) * self.rate/100
            if random.randint(1, 2) == 1:
                #A-＞B
                self.A -= gosa
                self.B += gosa
                self.A -= gosa*x/100
                self.B -= gosa*x/100
            else:
                #A<-B
                self.A += gosa
                self.B -= gosa
                self.A -= gosa*x/100
                self.B -= gosa*x/100
        return [int(self.A),int(self.B)]

    #変更１：資産が多い方を不利に、低い方は有利に働くルールを追加する
    def assistance_transaction(self):
        #資産が750より大きくなったら支出を2倍にする。250より少なくなったら補助資金100を追加
        for i in range(self.N):
            gosa = min(self.A,self.B) * self.rate/100
            if random.randint(1, 2) == 1:
                #A->B
                if self.A > 750:
                    self.A -= 2*gosa
                    self.B += 2*gosa
                else:
                    self.A -= gosa
                    self.B -= gosa
            else:
                if self.B > 750:
                    self.A += 2*gosa
                    self.B -= 2*gosa
                else:
                    self.A += gosa
                    self.B -= gosa
            if self.A < 250:
                self.A += 100
            if self.B < 250:
                self.B += 100
        return [int(self.A),int(self.B)]
    
    def ex_transaction(self):
        #N回取引するメソッド
        #変更点３：取引を資産が多いほうのN%として行う
        for i in range(self.N):
            gosa = max(self.A,self.B) * self.rate/100
            if random.randint(1, 2) == 1:
                #A-＞B
                self.A -= gosa
                self.B += gosa
            else:
                #A<-B
                self.A += gosa
                self.B -= gosa
        return [int(self.A),int(self.B)]

def main():
    
    for i in range(10):
        detaset4 = TradeInf(100,1000,10,100) #A=100,B=1000,rate=10%,N=100回 
        print(f'detaset4:{detaset4.social_transaction(10)}') #税率10%


    for i in range(10):
        detaset5 = TradeInf(100, 1000, 10, 100) #A=100,B=1000,rate=10%,N=100回 
        print(f'detaset5:{detaset5.assistance_transaction()}')

    for i in range(10):
        detaset6 = TradeInf(100, 1000, 10, 100) #A=100,B=1000,rate=10%,N=100回 
        print(f'detaset6:{detaset6.ex_transaction()}')

if __name__ == "__main__":
    main()

'''
結果はランダムに与えられる。結果の一例を以下に示す。
今回使用したdetaset4~6において
A=100,B=1000,rate=10%,N=100回
は固定して検証を行った。

detaset4:[17, 981]
detaset4:[9, 992]
detaset4:[5, 1023]
detaset4:[17, 1035]
detaset4:[7, 1007]
detaset4:[17, 1000]
detaset4:[49, 890]
detaset4:[306, 446]
detaset4:[26, 957]
detaset4:[21, 958]
階層化の導入について１回の取引で両方x%徴収される。
detaset1と比べると両者の資産が減り、特に資産が少なかったAの資産がより少なくなっている。
detaset5:[669, 261]
detaset5:[632, 255]
detaset5:[551, 327]
detaset5:[640, 347]
detaset5:[360, 327]
detaset5:[650, 295]
detaset5:[522, 294]
detaset5:[451, 295]
detaset5:[758, 252]
detaset5:[638, 327]
detaset5では資産が750を超えたらの支出を２倍、250より少なくなったら資産に補助金として100を追加する。ことを行った。
結果からすると補助金の額が現状多すぎること、支出を２倍だと資産が多いほうが不利に働きすぎる。
補助金と支払いの割合を変化させることで均等化できると考えた。
detaset6:[-250, 1350]
detaset6:[776, 323]
detaset6:[196, 903]
detaset6:[740, 359]
detaset6:[-550, 1650]
detaset6:[-1364, 2464]
detaset6:[516, 583]
detaset6:[722, 377]
detaset6:[647, 452]
detaset6:[-916, 2016]
単純に資産が多いほうのN%とすると片方が大きく損をするか得をするかという結果となった。
'''